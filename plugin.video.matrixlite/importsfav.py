import urllib, os, xbmc, xbmcgui

data_folder = xbmc.translatePath(os.path.join('special://home','userdata','addon_data','plugin.program.super.favourites','Super Favourites','Matrix IPTV'))
Url = 'http://kodimaster.com/matrixlite/'
File = ['favourites.xml']

# os.makedirs(data_folder)

def download(url, dest, dp = None):
    if not dp:
        dp = xbmcgui.DialogProgress()
        dp.create("Importing Super Favourites","Downloading & Copying File",' ', ' ')
    dp.update(0)
    urllib.urlretrieve(url,dest,lambda nb, bs, fs, url=url: _pbhook(nb,bs,fs,url,dp))
 
def _pbhook(numblocks, blocksize, filesize, url, dp):
    try:
        percent = min((numblocks*blocksize*100)/filesize, 100)
        dp.update(percent)
    except:
        percent = 100
        dp.update(percent)
    if dp.iscanceled(): 
        raise Exception("Canceled")
        dp.close()
		


if os.path.isdir(data_folder):
    for file in File:
        url = Url + file
        fix = xbmc.translatePath(os.path.join( data_folder, file))
        download(url, fix)
	
else:
    for file in File:
        os.makedirs(data_folder)
	url = Url + file
	fix = xbmc.translatePath(os.path.join( data_folder, file))
	download(url, fix)
	
d = xbmcgui.Dialog()
d.ok('Matrix IPTV', 'Super Favourites Imported')
