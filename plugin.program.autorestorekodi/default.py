# -*- coding: utf-8 -*-

import xbmc, xbmcplugin, xbmcgui, xbmcaddon
import urllib, urllib2, re, os, sys, shutil, zipfile

id = 'plugin.program.autorestorekodi'
addon = xbmcaddon.Addon(id)
home = addon.getAddonInfo('path')
icon = xbmc.translatePath(os.path.join(home, 'icon.png'))
src = 'https://github.com/aznsrp/repository.aznsrp/raw/master/MyFolder/addons_zip/'
dest = xbmc.translatePath('special://home/addons/')
autorestore = xbmc.translatePath(os.path.join(home, 'autorestorekodi.py'))
gui_orig = xbmc.translatePath('special://home/userdata/guisettings.xml')
gui_bk = xbmc.translatePath(os.path.join(home, 'guisettings.xml'))

def make_request(url):
	req = urllib2.Request(url)
	req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0')
	response = urllib2.urlopen(req)
	link = response.read()
	response.close()  
	return link

def attention(message, title = '[COLOR yellow]auto restore[/COLOR][COLOR cyan] Kodi[/COLOR]'):
	xbmcgui.Dialog().ok(title, message)

def main_menu():	
	try:
		createAddons()
		if os.path.exists(gui_bk):
			os.remove(gui_orig)
			shutil.move(gui_bk, gui_orig)	
		if os.path.isfile(autorestore):
			os.remove(autorestore)
		attention('Please force close Kodi.\nEnjoy!')			
	except:
		attention('[COLOR red]Something is wrong.[/COLOR] Please reboot Kodi and try again.')		

def createAddons():
	add_ons = next(os.walk(dest))[1]
	content = make_request(src)
	match = re.compile('<span.+?><a.+?title="(.+?)">').findall(content)
	dp = xbmcgui.DialogProgress()
	dp.create('[COLOR white]auto[/COLOR][COLOR red]restore[/COLOR][COLOR yellow]kodi[/COLOR]', 'Please [COLOR red]do not[/COLOR] press [COLOR red]Cancel[/COLOR]. Restoring Kodi...')			
	i = 0
	while i < 101:	
		for dirname in match:
			if not (os.path.splitext(dirname)[0]) in add_ons:
				dest_dirname = os.path.join(dest, dirname)
				percent = int((i / 400.0) * 100)	
				dp.update(percent, "", "", "")
				try:
					urllib.urlretrieve((src + dirname), dest_dirname)
					zf = zipfile.ZipFile(dest_dirname)
					zf.extractall(dest)
					zf.close()
					os.remove(dest_dirname)
				except:
					pass
			i = i + 1				
			if dp.iscanceled():
				return			
	dp.close()
	del dp		
		
def get_params():
	param = []
	paramstring = sys.argv[2]
	if len(paramstring)>= 2:
		params = sys.argv[2]
		cleanedparams = params.replace('?', '')
		if (params[len(params)-1] == '/'):
			params = params[0:len(params)-2]
		pairsofparams = cleanedparams.split('&')
		param = {}
		for i in range(len(pairsofparams)):
			splitparams = {}
			splitparams = pairsofparams[i].split('=')
			if (len(splitparams)) == 2:
				param[splitparams[0]] = splitparams[1]
	return param

def add_dir(name, url, mode, iconimage):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(name) + "&iconimage=" + urllib.quote_plus(iconimage)
	ok = True
	liz = xbmcgui.ListItem(name, iconImage = "DefaultFolder.png", thumbnailImage = iconimage)
	liz.setInfo( type = "Video", infoLabels = { "Title": name } )
	ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = u, listitem = liz, isFolder = True)
	return ok
	
params = get_params()
url = None
name = None
mode = None
iconimage = None

try:
	url = urllib.unquote_plus(params["url"])
except:
	pass
try:
	name = urllib.unquote_plus(params["name"])
except:
	pass
try:
	mode = int(params["mode"])
except:
	pass	
try:
	iconimage = urllib.unquote_plus(params["iconimage"])
except:
	pass  

print "Mode: " + str(mode)
print "URL: " + str(url)
print "Name: " + str(name)
print "iconimage: " + str(iconimage)

if mode == None or url == None or len(url) < 1:
	main_menu()

xbmcplugin.endOfDirectory(int(sys.argv[1]))