import xbmc

osWin = xbmc.getCondVisibility('system.platform.windows')
osAndroid = xbmc.getCondVisibility('system.platform.android')

if osAndroid:	# Amazon Fire TV/Fire Stick, Android
	xbmc.executebuiltin("StartAndroidActivity(com.viettv24.iptv)")

if osWin:		# Chrome Launcher for Win/OSX/Linux
	xbmc.executebuiltin("ActivateWindow(10001, plugin://plugin.program.chrome.launcher/?kiosk=no&mode=showSite&stopPlayback=no&url=http%3a%2f%2fwww.viettv24.com%2fmain%2f%3flanguage%3dvi)")