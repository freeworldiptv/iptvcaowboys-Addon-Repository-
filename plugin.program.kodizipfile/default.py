# -*- coding: utf-8 -*-

'''
Copyright (C) 2015                                                     

This program is free software: you can redistribute it and/or modify   
it under the terms of the GNU General Public License as published by   
the Free Software Foundation, either version 3 of the License, or      
(at your option) any later version.                                    

This program is distributed in the hope that it will be useful,        
but WITHOUT ANY WARRANTY; without even the implied warranty of         
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
GNU General Public License for more details.                           

You should have received a copy of the GNU General Public License      
along with this program. If not, see <http://www.gnu.org/licenses/>  
'''                                                                           

import xbmc, xbmcplugin, xbmcgui, xbmcaddon
import urllib, urllib2, re, os, sys, zipfile, shutil

id = 'plugin.program.kodizipfile'
addon = xbmcaddon.Addon(id)
profile = addon.getAddonInfo('profile')
home = addon.getAddonInfo('path')
fanart = xbmc.translatePath(os.path.join(home, 'fanart.jpg'))
icon = xbmc.translatePath(os.path.join(home, 'icon.png'))
src_folder = xbmc.translatePath('special://home/addons')
dst_folder = addon.getSetting('dst')
src_zip = addon.getSetting('srczip')
dst_unzip = addon.getSetting('dstunzip')

def attention(message, title = "*[COLOR cyan]kodizipfile[/COLOR]"):
	xbmcgui.Dialog().ok(title, message)

def main():
	add_dir("[COLOR yellow]Create[/COLOR] zipfiles for Kodi's addons folder", "createzipfiles", 1, icon, fanart)
	add_dir("[COLOR lime]Unzip[/COLOR] zipfiles in a folder", "unzipfiles", 2, icon, fanart)	

def create_zipfiles():
	if len(dst_folder) > 0:
		dp = xbmcgui.DialogProgress()
		dp.create("*[COLOR cyan]kodizipfile[/COLOR]", "Please wait. [COLOR magenta]Creating zipfiles...[/COLOR]")
		if os.path.exists(dst_folder):
			shutil.rmtree(dst_folder)
		os.mkdir(dst_folder)		
		i = 0
		while i < 101:	
			os.chdir(src_folder)
			add_ons = next(os.walk(src_folder))[1]
			for add_on in add_ons:
				percent = int((i / 100.0) * 100)
				dp.update(percent, "", "", "")
				xbmc.sleep(1000)
				if add_on == 'packages':
					pass
				else:
					target_zipfile = (dst_folder + '\\' + add_on + '.zip')
					zf = zipfile.ZipFile(target_zipfile, 'w')
					for dirname, subdirs, files in os.walk(add_on):		
						zf.write(dirname)
						for filename in files:
							zf.write(os.path.join(dirname, filename))
				if dp.iscanceled():
					break
				i = i + 1
			zf.close()
		dp.close()
		del dp								
		attention('Zipfiles have been saved in [COLOR magenta]' + dst_folder + '[/COLOR]\nDone. Enjoy!')
		sys.exit()
	else:
		addon.openSettings()

def un_zip():
	if len(src_zip) > 0 and len(dst_unzip) > 0:
		dp = xbmcgui.DialogProgress()
		dp.create("*[COLOR cyan]kodizipfile[/COLOR]", "Please wait. [COLOR magenta]Unzipping files...[/COLOR]")
		if os.path.exists(dst_unzip):
			shutil.rmtree(dst_unzip)
		os.mkdir(dst_unzip)		
		i = 0
		while i < 101:		
			os.chdir(src_zip)
			for zip in os.listdir(src_zip):
				percent = int((i / 100.0) * 100)
				dp.update(percent, "", "", "")
				xbmc.sleep(1000)			
				if zip.endswith('.zip'):
					zf = zipfile.ZipFile(src_zip + zip)
					zf.extractall(dst_unzip)
				if dp.iscanceled():
					break
				i = i + 1					
			zf.close
		attention('Unzipped files have been saved in [COLOR magenta]' + dst_unzip + '[/COLOR]\nDone. Enjoy!')
		sys.exit()
	else:
		addon.openSettings()	

def get_params():
	param = []
	paramstring = sys.argv[2]
	if len(paramstring)>= 2:
		params = sys.argv[2]
		cleanedparams = params.replace('?', '')
		if (params[len(params)-1] == '/'):
			params = params[0:len(params)-2]
		pairsofparams = cleanedparams.split('&')
		param = {}
		for i in range(len(pairsofparams)):
			splitparams = {}
			splitparams = pairsofparams[i].split('=')
			if (len(splitparams)) == 2:
				param[splitparams[0]] = splitparams[1]
	return param

def add_dir(name, url, mode, iconimage, fanart):
	u = (	
			sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + 
			"&name=" + urllib.quote_plus(name) + "&iconimage=" + urllib.quote_plus(iconimage)
		)	
	ok = True
	liz = xbmcgui.ListItem(name, iconImage = "DefaultFolder.png", thumbnailImage = iconimage)
	liz.setInfo( type = "Video", infoLabels = { "Title": name } )
	liz.setProperty('fanart_image', fanart)
	ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = u, listitem = liz, isFolder = True)
	return ok

params = get_params()
url = None
name = None
mode = None
iconimage = None

try:
	url = urllib.unquote_plus(params["url"])
except:
	pass
try:
	name = urllib.unquote_plus(params["name"])
except:
	pass
try:
	mode = int(params["mode"])
except:
	pass
try:
	iconimage = urllib.unquote_plus(params["iconimage"])
except:
	pass  
 
print "Mode: " + str(mode)
print "URL: " + str(url)
print "Name: " + str(name)
print "iconimage: " + str(iconimage)

if mode == None or url == None or len(url) <1:
	main()
elif mode == 1:
	create_zipfiles()
elif mode == 2:
	un_zip()
		
xbmcplugin.endOfDirectory(int(sys.argv[1]))